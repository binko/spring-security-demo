package com.example.spring.security.demo.config;

import com.example.spring.security.demo.config.validate.ValidateCodeGenerator;
import com.example.spring.security.demo.config.validate.image.ImageCodeGenerator;
import com.example.spring.security.demo.config.validate.sms.sender.DefaultSmsCoderSender;
import com.example.spring.security.demo.config.validate.sms.sender.SmsCodeSender;
import com.example.spring.security.demo.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ValidateCodeBeanConfig {

    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    @ConditionalOnMissingBean(SmsCodeSender.class)
    public SmsCodeSender smsCodeSender(){
        //如果找不到配置的Bean 则用默认的
        return new DefaultSmsCoderSender();
    }

    /**
     * 如果没有imageCodeGenerator的类，则加载
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(name = "imageCodeGenerator")
    public ValidateCodeGenerator imageCodeGenerator(){
        ImageCodeGenerator codeGenerator = new ImageCodeGenerator();
        codeGenerator.setSecurityProperties(securityProperties);
        return codeGenerator;
    }


}
