package com.example.spring.security.demo.config.validate;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ValidateCode {

    private String code;
    private LocalDateTime expireTime;

}
