package com.example.spring.security.demo.config.validate;


import org.springframework.security.core.AuthenticationException;

public class ValidateCodeException extends AuthenticationException {

    public ValidateCodeException(String name){
        super(name);
    }
}
