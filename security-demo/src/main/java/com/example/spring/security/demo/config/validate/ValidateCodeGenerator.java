package com.example.spring.security.demo.config.validate;

import com.example.spring.security.demo.config.validate.ValidateCode;
import org.springframework.web.context.request.ServletWebRequest;

public interface ValidateCodeGenerator {
    ValidateCode generate(ServletWebRequest request);
}
