package com.example.spring.security.demo.config.validate.sms;

import com.example.spring.security.demo.config.validate.ValidateCode;
import com.example.spring.security.demo.config.validate.ValidateCodeGenerator;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

import java.time.LocalDateTime;
import java.util.Random;

@Component
public class SmsCodeGenerator implements ValidateCodeGenerator {


    @Override
    public ValidateCode generate(ServletWebRequest request) {
        String code = RandomStringUtils.randomNumeric(6);
        return new ValidateCode(code,LocalDateTime.now());
    }
}
