package com.example.spring.security.demo.config.validate.sms.sender;

public class DefaultSmsCoderSender implements SmsCodeSender {

    @Override
    public void send(String mobile, String code) {
        System.out.println("手机：" + mobile + "，发送短信验证码：" + code);
    }

}
