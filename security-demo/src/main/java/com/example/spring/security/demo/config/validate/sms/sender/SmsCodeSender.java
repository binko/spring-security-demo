package com.example.spring.security.demo.config.validate.sms.sender;

public interface SmsCodeSender {

    public void send(String mobile, String code);
}
