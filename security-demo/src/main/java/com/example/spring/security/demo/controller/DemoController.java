package com.example.spring.security.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class DemoController {


    @GetMapping("/sayHello")
    @ResponseBody
    public String sayHello(String name){
        return "Hello :"+name;
    }

    @GetMapping("/index")
    @ResponseBody
    public String loginUser(String name){
        return "Index :"+name;
    }

    @GetMapping("/fail")
    public String fail(){
        return "fail";
    }

    @GetMapping("/success")
    public String success(){
        return "success";
    }




}
